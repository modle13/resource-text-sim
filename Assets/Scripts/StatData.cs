public class StatData {
    public string name;
    public int minValue;
    public int maxValue;
    public string condition;
    public int conditionTrigger = 0;
    public int consumeAmount = 1;
    public int consumeTrigger = 1;
    public int replenishValue = 99;
    public string consumes;
    public int consumesValue;

    public override string ToString() {
        return "name: " + name + "; " +
            "minValue: " + minValue + "; " +
            "maxValue: " + maxValue + "; " +
            "condition: " + condition + "; " +
            "conditionTrigger: " + conditionTrigger + "; " +
            "consumeAmount: " + consumeAmount + "; " +
            "consumeTrigger: " + consumeTrigger + "; " +
            "replenishValue: " + replenishValue + "; " +
            "consumes: " + consumes + "; " +
            "consumesValue: " + consumesValue;
    }
}
