using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// https://catlikecoding.com/unity/tutorials/object-management/multiple-scenes/

[CreateAssetMenu]
public class VillagerBucket : VillagerStockCommonScrollRect {

    Scene VillagerCanvasScene;
    public Transform villagersContent;
    public VillagerData villagerData;
    private string[] names;
    public ScrollRectHelper scrollRectHelper;
    List<StatData> initialStats;
    private int minTimeBetweenNewVillagers = 1000;
    private int maxTimeBetweenNewVillagers = 1500;
    private int timeToNextVillager = 1200;
    private int timeSinceNewVillager = 0;
    private FileHandler fileHandler;
    private int villagerCount = 0;
    private bool loaded;
    private VillageCenter villageCenter;

    public void Init(List<StatData> _initialStats) {
        fileHandler = GameObject.Find("FileHandler").GetComponent<FileHandler>();
        villageCenter = GameObject.Find("VillageCenter").GetComponent<VillageCenter>();
        initialStats = _initialStats;
        scrollRectHelper = ScrollRectHelper.CreateInstance("ScrollRectHelper") as ScrollRectHelper;
        // LoadPrefabs();
        LoadPrefabs("Villager");
        CreateVillagerRect();
        LoadVillagerData();
        LoadVillagerNames();
        GenerateVillagers();
        loaded = true;
    }

    public void CreateVillagerRect() {
        VillagerCanvasScene = SceneManager.CreateScene("VillagerCanvas");
        GameObject instance = Instantiate(prefabs["VillagerCanvas"]) as GameObject;;
        SceneManager.MoveGameObjectToScene(
            instance, VillagerCanvasScene
        );
        villagersContent = instance.transform.Find("VillagerRect").Find("Viewport").Find("Content");
        toggler = instance.transform.Find("VillagerRect").gameObject;
    }

    private void LoadVillagerData() {
        if (!fileHandler.loaded) {
            LoadVillagerData();
        }
        string jsonString = "";
        foreach (KeyValuePair<string, string> entry in fileHandler.data["villagerData"]) {
            jsonString = entry.Value;
            break;
        }
        if (jsonString != "") {
            villagerData = JsonUtility.FromJson<VillagerData>(jsonString);
        }
        Debug.Log("target villager count is " + villagerData.villagerCount);

        // load names list
        Debug.Log("loading villagers name list");
    }

    private void LoadVillagerNames() {
        // this is a dict of a single entry because everything else needs the dict
        foreach (KeyValuePair<string, string> entry in fileHandler.data["villagerNames"]) {
            names = entry.Value.Split(new[] { "\n", "\r", "\r\n" }, System.StringSplitOptions.None);
            break;
        }
    }

    private void GenerateVillagers() {
        Debug.Log("generating villagers");
        // for some reason the content is set at -50k pixels at this point, and fixes itself during villager add during gameplay
        while (GetCount() < villagerData.villagerCount) {
            if (!DataIsReady()) {
                continue;
            }
            // trick ManagePopulation function to generate the needed starting villagers
            timeSinceNewVillager = timeToNextVillager;
            ManagePopulation();
        }
    }

    private bool DataIsReady() {
        if (villageCenter.villagerBucket != null) {
            if (villageCenter.villagerBucket.GetData() == null) {
                Debug.Log("data not ready inside VillagerBucket GenerateVillagers");
                return false;
            }
        }
        return true;
    }

    public void ManagePopulation() {
        timeSinceNewVillager++;
        if (timeSinceNewVillager > timeToNextVillager) {
            CreateVillager(GetVillagerName());
            timeSinceNewVillager = 0;
            SetTimeToNextVillager();
        }
    }

    private void SetTimeToNextVillager() {
        timeToNextVillager = Random.Range(minTimeBetweenNewVillagers, maxTimeBetweenNewVillagers);
    }

    private void CreateVillager(string villagerName) {
        Vector3 villagerPos = Vector3.zero;
        villagerPos.y = 0;
        Debug.Log("instantiating villager " + villagerName);
        GameObject instance = Instantiate(prefabs["Villager"], villagerPos, Quaternion.identity) as GameObject;

        scrollRectHelper.SetObjectParent(villagersContent, instance.transform);

        Villager villager = instance.GetComponent<Villager>();
        villager.id = scrollRectHelper.GetChildrenCount(villagersContent);
        villager.label = villagerName;
        villager.SetStats(initialStats);
        villager.Initialize();

        // looks like content object is getting set -50k
        scrollRectHelper.UpdateContentSize(villagersContent, instance);
        scrollRectHelper.SortChildren(villagersContent);
        villageCenter.log.Add("activity", villagerName + " joins your village");
    }

    private string GetVillagerName() {
        string villagerName = "";
        while (villagerName.Equals("")) {
            villagerName = names[Random.Range(0, names.Length)].Trim();
        }
        return villagerName;
    }

    public VillagerData GetData() {
        return villagerData;
    }

    public int GetCount() {
        return villagersContent.childCount;
    }

    public Transform GetVillagers() {
        return villagersContent;
    }
}
