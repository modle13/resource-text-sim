using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using TMPro;

public class Villager : MonoBehaviour {

    public int id;
    public string label;
    public bool assigned = false;
    public bool dead = false;
    private bool processed = false;
    public List<string> conditions = new List<string>();
    public Dictionary<string, Stat> stats = new Dictionary<string, Stat>();
    public Fixture assignment;
    private ConditionProcessor conditionProcessor;
    public VillagerData data;
    TextMeshProUGUI reprText;
    private int tick = 30;
    public List<string> vitalStats = new List<string>(new string[] {"energy", "warmth"});
    string deathMessageTemplate = "{0:s} has {1:s} to death";
    public Dictionary<string, string> deathMessages = new Dictionary<string, string>() {
        {"warmth", "frozen"},
        {"energy", "starved"}
    };
    private VillageCenter villageCenter;

    private void Awake() {
        conditionProcessor = new ConditionProcessor(this);
        villageCenter = GameObject.Find("VillageCenter").GetComponent<VillageCenter>();
        if (villageCenter.villagerBucket != null) {
            data = villageCenter.villagerBucket.GetData();
        }
        if (data == null) {
            Debug.Log("data not ready inside villager awake");
        }
        reprText = transform.Find("Repr")?.GetComponent<TextMeshProUGUI>();
        villageCenter = GameObject.Find("VillageCenter").GetComponent<VillageCenter>();
    }

    public void Initialize() {
        TextMeshProUGUI labelText = transform.Find("Label").GetComponent<TextMeshProUGUI>();
        labelText.text = label;
    }

    public void SetStats(List<StatData> data) {
        foreach (StatData entry in data) {
            Debug.Log("loading stat " + entry.name);
            stats[entry.name] = new Stat(entry);
        }
    }

    private void Update() {
        reprText.text = Repr();
        if (dead) {
            return;
        }
        if (IsEligibleForProcessing() && !processed) {
            conditionProcessor.Update();
            processed = true;
        }
        CheckStats();
    }

    private bool IsEligibleForProcessing() {
        if (CheckTick()) {
            this.processed = false;
            return false;
        }
        if (this.processed) {
            return false;
        }
        return true;
    }

    public bool CheckTick() {
        bool tickIsEligible = (
            (int)(Time.time * 10)
            % (tick / 5)
            == 0
        );
        return tickIsEligible;
    }

    public string Repr() {
        // format this more dynamically since stats are loaded from text files
        return string.Format(
            "W:{0:d} | E:{1:d} | T:{2:d} | C:{3:d} | B:{4:s}",
            stats["warmth"].value,
            stats["energy"].value,
            stats["tool"].value,
            stats["clothing"].value,
            // assignment may be null
            assignment?.GetLabel()
        );
    }

    private void CheckStats() {
        foreach (KeyValuePair<string, Stat> entry in stats) {
            Kill(entry.Value);
            Replenish(entry.Value);
            SetCondition(entry.Value);
        }
    }

    private void Kill(Stat stat) {
        if (!vitalStats.Contains(stat.name)) {
            return;
        }
        if (stat.value <= data.deathTrigger) {
            dead = true;
            // remove from building
            if (assignment != null) {
                assignment?.Unassign(this);
                assignment = null;
            }
            villageCenter.log.Add("activity", string.Format(deathMessageTemplate, label, deathMessages[stat.name]));
        }
    }

    public void Replenish(Stat stat) {
        if (!villageCenter.stockBucket.HaveEnough(stat.consumes)) {
            return;
        }
        bool addStat = false;
        if (stat.IsTriggered()) {
            addStat = villageCenter.stockBucket.RemoveFromStock(stat.consumes);
        }
        if (addStat) {
            stat.Replenish();
        }
    }

    private void SetCondition(Stat stat) {
        if (stat.value <= stat.conditionTrigger) {
            conditions.Add(stat.condition);
        }
    }

    public void Assign(Fixture fixture) {
        assigned = true;
        assignment = fixture;
    }

    public void Unassign() {
        assigned = false;
        assignment = null;
    }

    private class ConditionProcessor {
        private Villager villager;
        private EventHandler eventHandler;

        public ConditionProcessor(Villager _villager) {
            villager = _villager;
            eventHandler = GameObject.Find("EventHandler").GetComponent<EventHandler>();
        }

        public void Update() {
            Default();
            Assigned();
            Special();
        }

        private void Default() {
            AdjustStat("clothing", villager.data.baseStatDelta);
            AdjustStat("energy", villager.data.baseStatDelta);
            AdjustStat("warmth", (villager.data.baseStatDelta * villager.data.warmthIdleMultiplier));
        }

        private void Assigned() {
            if (villager.assignment != null) {
                // use more energy if assigned
                AdjustStat("energy", villager.data.baseStatDelta);
                // add warmth if assigned
                AdjustStat("warmth", -villager.data.baseStatDelta * villager.data.warmthWorkMultiplier);
            }
        }

        public void Event() {
            AdjustStat("clothing", eventHandler.GetEventEffect("clothing", "villager"));
            AdjustStat("energy", eventHandler.GetEventEffect("energy", "villager"));
            AdjustStat("warmth", eventHandler.GetEventEffect("warmth", "villager"));
            if (ToolUsed()) {
                AdjustStat("tool", eventHandler.GetEventEffect("tool", "villager"));
            }
        }

        private bool ToolUsed() {
            return villager.stats["tool"].value > 0;
            // some conditional for whether villager is producing
            // must be assigned and target fixture must be active
        }

        private void Special() {
            if (villager.stats["clothing"].value > 0) {
                AdjustStat("warmth", (villager.data.baseStatDelta * villager.data.clothingMultiplier));
            }
        }

        private void AdjustStat(string what, int amount) {
            villager.stats[what].AdjustValue(amount);
        }
    }
}
