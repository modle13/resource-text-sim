using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System;
using UnityEngine.Networking;

public class FileHandler : MonoBehaviour {

    public Dictionary<string, Dictionary<string, string>> data = new Dictionary<string, Dictionary<string, string>>();
    public bool loaded = false;

    public void Awake() {
        Config.Init();
        LoadThese();
        loaded = true;
    }

    private void LoadThese() {
        foreach (KeyValuePair<string, string> entry in Config.data) {
            data.Add(entry.Key, new Dictionary<string, string>());
            // Debug.Log("loading " + entry.Key);
            string namePath = System.IO.Path.Combine(Application.streamingAssetsPath, entry.Value);
            if (Config.dataCollectionTypes[entry.Key] == "singleFile") {
                Debug.Log(entry.Key + " is singleFile; path is " + namePath);
                LoadData(namePath, entry.Key, "single");
            }
            if (Config.dataCollectionTypes[entry.Key] == "listOfFiles") {
                Dictionary<string, string> filePaths = GetDataFilePaths(namePath, Config.dataLists[entry.Key]);
                Debug.Log(entry.Key + " is listOfFiles; path is " + namePath);
                foreach (KeyValuePair<string, string> filePathEntry in filePaths) {
                    Debug.Log(filePathEntry.Key + ": " + filePathEntry.Value);
                    LoadData(filePathEntry.Value, entry.Key, filePathEntry.Key);
                }
            }
            if (Config.dataCollectionTypes[entry.Key] == "listOfDirs") {
                Dictionary<string, string> filePaths = GetDataFilePaths(namePath, Config.dataLists[entry.Key]);
                Debug.Log(entry.Key + " is listOfDirs; path is " + namePath);
                foreach (KeyValuePair<string, string> filePathEntry in filePaths) {
                    Debug.Log("looking up " + entry.Key + " in Config.expectedFiles");
                    foreach (string expectedEntry in Config.expectedFiles[entry.Key]) {
                        Debug.Log(filePathEntry.Key + ": " + filePathEntry.Value);
                        LoadData(filePathEntry.Value + "/" + expectedEntry, entry.Key, filePathEntry.Key + "_" + expectedEntry);
                    }
                }
            }
        }
        string dataKeys = "";
        foreach (KeyValuePair<string, Dictionary<string, string>> entry in data) {
            dataKeys += entry.Key + "; ";
        }
        Debug.Log(dataKeys);
    }

    public Dictionary<string, string> GetDataFilePaths(string filePath, List<string> filenames) {
        Dictionary<string, string> loadedPaths = new Dictionary<string, string>();
        foreach (string entry in filenames) {
            loadedPaths[entry] = filePath + "/" + entry;
        }
        return loadedPaths;
    }

    private void LoadData(string filePath, string dataName, string dataKey) {
        string message = "LoadStreamingAsset filepath is " + filePath;
        Debug.Log(message);

        try {
            StartCoroutine(RequestRoutine(filePath, dataName, dataKey));
        } catch (Exception e) {
            Debug.Log("failed to do retrieve data files at " + filePath);
        }
    }

    private IEnumerator RequestRoutine(string url, string dataName, string dataKey) {
        Debug.Log("loading file path " + url);
        UnityWebRequest request = UnityWebRequest.Get(url);
        yield return request.SendWebRequest();
        data[dataName][dataKey] = request.downloadHandler.text;

        Debug.Log("data in request routine is " + url + " - " + data[dataName][dataKey]);
    }
}
