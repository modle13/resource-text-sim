using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using TMPro;

// https://catlikecoding.com/unity/tutorials/object-management/multiple-scenes/

[CreateAssetMenu]
public class FixtureBucket : BaseScriptableScrollRect {

    private List<string> availableTypes = new List<string>();
    public bool fixtureCreated;
    public string fileHandlerDataType;

    public override void GenerateObjects(List<string> objectList, string prefabType, string elementType, List<string> config) {
        availableTypes = config;
        fileHandlerDataType = elementType;
        foreach (string entry in objectList) {
            Debug.Log("creating fixture: " + entry);
            CreateObject(entry, prefabType, "standard");
        }
    }

    private void CreateObject(string typeName, string prefabType, string phase) {
        Debug.Log("typeName is " + typeName + "; prefabType is " + prefabType + "; phase is " + phase);
        Vector3 objectPos = Vector3.zero;
        GameObject instance = Instantiate(prefabs[prefabType], objectPos, Quaternion.identity) as GameObject;

        float yOffset = content.GetComponent<RectTransform>().sizeDelta.y;
        scrollRectHelper.SetObjectPosition(instance.transform, scrollRectHelper.GetChildrenCount(content), yOffset);
        scrollRectHelper.SetObjectParent(content, instance.transform);

        Fixture fixture = instance.GetComponent<Fixture>();
        fixture.id = scrollRectHelper.GetChildrenCount(content);
        fixture.phase = phase;
        fixture.elementType = fileHandlerDataType;
        fixture.nextForm = typeName;
        fixture.SetLabel(typeName);

        scrollRectHelper.UpdateContentSize(content, instance);
        // this function call causes objects to move way off the screen
        // scrollRectHelper.SortChildren(content);
    }

    public void ManageInfrastructure() {
        // careful with infinite loops here
        foreach (Transform t in content) {
            Fixture fixture = t.GetComponent<Fixture>();
            // inherent delay in Destroy?
            if (fixture.convert) {
                ConvertWorksite(fixture);
                break;
            }
            if (fixture.salvage) {
                SalvageFixture(t);
                scrollRectHelper.SortChildren(content);
                // salvage does a destroy
                // don't want to continue looping when the list has been modified
                break;
            }
        }
    }

    private void ConvertWorksite(Fixture fixture) {
        // creates a new fixture based on properties of current
        CreateObject(fixture.nextForm, "Building", "standard");
        fixture.convert = false;
        fixture.salvage = true;
        fixtureCreated = true;
    }

    private void SalvageFixture(Transform target) {
        // unassigns villagers destroys the fixture gameObject
        Fixture fixture = target.GetComponent<Fixture>();
        fixture.UnassignAll();
        // Fixture is the script; we want to destroy the gameObject of the script's parent
        Destroy(target.gameObject);
    }

    public void AddWorksite(string typeName) {
        CreateObject(typeName, "Building", "worksite");
    }

    public void AddResearch(string typeName) {
        CreateObject(typeName, "Research", "researching");
    }

    public List<string> GetObjectList() {
        return availableTypes;
    }

}
