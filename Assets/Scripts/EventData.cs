public class EventData {
    public string label;
    public string description;
    public string type;
    public string effect;
    public string target;
    public int direction;

    public override string ToString() {
        return "label: " + label +
            "; description: " + description +
            "; type: " + type +
            "; effect: " + effect +
            "; target: " + target +
            "; direction: " + direction;
    }
}
