using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System;
using UnityEngine.Networking;

public class ConfigFileDeserializer : MonoBehaviour {
    private string[] names;
    private FileHandler fileHandler;
    private List<string> dataNames = new List<string>(new string[] {"buildings", "research", "events", "stats"});
    private List<string> loaded = new List<string>();

    public void Start() {
        fileHandler = GameObject.Find("FileHandler").GetComponent<FileHandler>();
    }

    public void Update() {
        if (fileHandler.data.ContainsKey("villagerNames") && names == null && !loaded.Contains("villagerNames")) {
            SplitVillagerNames();
            Debug.Log("villagerNames loaded");
            loaded.Add("villagerNames");
        }
        foreach (string entry in dataNames) {
            if (fileHandler.data.ContainsKey(entry) && fileHandler.data[entry].Count != 0 && !loaded.Contains(entry)) {
                ProcessData(fileHandler.data[entry]);
                Debug.Log(entry + " loaded");
                loaded.Add(entry);
            }
        }
    }

    private void SplitVillagerNames() {
        // this is a dict of a single entry because everything else needs the dict
        foreach (KeyValuePair<string, string> entry in fileHandler.data["villagerNames"]) {
            names = entry.Value.Split(new[] { "\n", "\r", "\r\n" }, System.StringSplitOptions.None);
            break;
        }
    }

    private void ProcessData(Dictionary<string, string> theData) {
        foreach (KeyValuePair<string, string> entry in theData) {
            Debug.Log("there's some jsons: " + entry.Key + ", " + entry.Value);
        }
        // load events
        // load buildings
        // load research
        // load stats
        //     EventData data = JsonUtility.FromJson<EventData>(jsonString);
        //     events.Add(new Event(data));
    }
}
