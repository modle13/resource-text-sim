﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class CustomButtonClick : MonoBehaviour {

    private Button button;
    private VillageCenter villageCenter;
    public string type;
    private TextMeshProUGUI dropdownSelection;
    private Properties props;
    private string buildCommand = "add_building";
    private string researchCommand = "add_research";

    void Start () {
        villageCenter = GameObject.Find("VillageCenter").GetComponent<VillageCenter>();
        button = gameObject.GetComponent<Button>();
        button.onClick.AddListener(TaskOnClick);
        props = GetComponent<Properties>();
    }

    void TaskOnClick() {
        if (type.Equals("add") || type.Equals("remove")) {
            villageCenter.UpdateAssignment(transform.parent, type);
            return;
        }
        if (type.Equals(buildCommand)) {
            Debug.Log("Clicked " + buildCommand + " for " + props.label);
            villageCenter.AddWorksite(props.label);
            return;
        }
        if (type.Equals(researchCommand)) {
            Debug.Log("Clicked " + researchCommand + " for " + props.label);
            villageCenter.AddResearch(props.label);
            return;
        }
        villageCenter.SetSelection(type);
    }
}
