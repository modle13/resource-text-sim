using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using TMPro;

public class VillageCenter : MonoBehaviour {
    public VillagerBucket villagerBucket;
    public FixtureBucket buildingBucket;
    public ButtonBucket buildButtonBucket;
    public StockBucket stockBucket;
    public ButtonBucket researchButtonBucket;
    public VillageMap villageMap;
    public Log log;
    public FixtureBucket researchBucket;
    private Dictionary<string, string> availableStats = new Dictionary<string, string>();
    private List<StatData> initialStats = new List<StatData>();
    private List<string> initialBuildingTypes = new List<string>(new string[] {"forester"});
    private List<string> initialResearchTypes = new List<string>();

    // private List<Object> contentContainers = new List<Object>();
    private Dictionary<string, ContentToggler> contentContainers = new Dictionary<string, ContentToggler>();
    // private List<string> allowedSelections = new List<string>(new string[] {"BuildButton", "Building", "ResearchButton", "Research", "LogsButton", "MapButton"});

    private FileHandler fileHandler;

    private bool loaded = false;
    private bool statsLoaded = false;

    public void Start() {
        fileHandler = GameObject.Find("FileHandler").GetComponent<FileHandler>();
        LoadEverything();
    }

    private void Update() {
        if (!loaded) {
            LoadEverything();
            return;
        }
        villagerBucket.ManagePopulation();
        buildingBucket.ManageInfrastructure();
        villageMap.Update();
        log.Update();
        // find a better way to signal update stocks when fixture is created
        if (buildingBucket.fixtureCreated) {
            Debug.Log("Fixture was created, updating stocks");
            foreach (Transform building in buildingBucket.GetContent()) {
                stockBucket.UpdateStocks(building.gameObject);
            }
            buildingBucket.fixtureCreated = false;
        }
    }

    public void LoadEverything() {
        // might have problems here with race conditions when web loading
        if (!DataLoaded()) {
            return;
        }
        // steps past this point should only run once
        LoadStats();
        log = Log.CreateInstance("Log") as Log;
        log.Init();
        contentContainers.Add("LogsButton", log);

        villagerBucket = VillagerBucket.CreateInstance("VillagerBucket") as VillagerBucket;
        villagerBucket.Init(initialStats);
        contentContainers.Add("VillagerButton", villagerBucket);

        stockBucket = StockBucket.CreateInstance("StockBucket") as StockBucket;
        stockBucket.Init();
        contentContainers.Add("StockButton", stockBucket);

        Debug.Log("generating building bucket");
        buildingBucket = FixtureBucket.CreateInstance("FixtureBucket") as FixtureBucket;
        buildingBucket.Init(initialBuildingTypes, "Building", "buildings", Config.buildings);
        contentContainers.Add("Building", buildingBucket);

        Debug.Log("generating build button bucket");
        buildButtonBucket = ButtonBucket.CreateInstance("ButtonBucket") as ButtonBucket;
        buildButtonBucket.Init(buildingBucket.GetObjectList(), "BuildButton", "not used", new List<string>());
        contentContainers.Add("BuildButton", buildButtonBucket);

        Debug.Log("generating research bucket");
        researchBucket = FixtureBucket.CreateInstance("FixtureBucket") as FixtureBucket;
        researchBucket.Init(initialResearchTypes, "Research", "research", Config.research);
        contentContainers.Add("Research", researchBucket);

        Debug.Log("generating research button bucket");
        researchButtonBucket = ButtonBucket.CreateInstance("ButtonBucket") as ButtonBucket;
        researchButtonBucket.Init(researchBucket.GetObjectList(), "ResearchButton", "not used", new List<string>());
        contentContainers.Add("ResearchButton", researchButtonBucket);

        villageMap = VillageMap.CreateInstance("VillageMap") as VillageMap;
        villageMap.Init();
        contentContainers.Add("MapButton", villageMap);

        foreach (Transform building in buildingBucket.GetContent()) {
            stockBucket.UpdateStocks(building.gameObject);
        }
        loaded = true;
        foreach (KeyValuePair<string, ContentToggler> entry in contentContainers) {
            entry.Value.SetContentView("off");
        }
    }

    private bool DataLoaded() {
        foreach (string entry in Config.dataCollectionTypes.Keys) {
            if (!fileHandler.data.ContainsKey(entry) || fileHandler.data[entry].Count == 0) {
                return false;
            }
        }
        return true;
    }

    public void SetSelection(string type) {
        // when this is triggered, set all other entries in that type's group to hidden
        Debug.Log("type is " + type);
        foreach (KeyValuePair<string, ContentToggler> entry in contentContainers) {
            Debug.Log(entry.Key);
            if (type.Equals(entry.Key)) {
                entry.Value.SetContentView("on");
            } else {
                entry.Value.SetContentView("off");
            }
        }
    }

    private void UpdateRectVisibility(List<BaseScriptableScrollRect> rects, string type) {
        foreach (BaseScriptableScrollRect bucket in rects) {
            bucket.visible = bucket.scrollRectType.Equals(type);
            // toggle toggles
            bucket.ToggleContentView();
            // check sets visibility based on current state
            // bucket.CheckContentView();
        }
    }

    private void LoadStats() {
        foreach (KeyValuePair<string, string> entry in fileHandler.data["stats"]) {
            string jsonString = "badstring";
            jsonString = entry.Value;
            if (jsonString != "badstring") {
                StatData data = JsonUtility.FromJson<StatData>(jsonString);
                Debug.Log(data.ToString());
                initialStats.Add(data);
            }
        }
    }

    public void AddWorksite(string target) {
        buildingBucket.AddWorksite(target);
    }

    public void AddResearch(string target) {
        researchBucket.AddResearch(target);
    }

    // needs to move to VillagerBucket
    public void UpdateAssignment(Transform fixture, string adjustmentType) {
        Fixture fixtureObj = fixture.GetComponent<Fixture>();
        string fixtureName = fixtureObj.GetLabel();
        foreach (Transform child in villagerBucket.GetVillagers()) {
            Villager villager = child.GetComponent<Villager>();

            if (villager.dead) {
                Unassign(fixtureObj, villager);
                continue;
            }

            if (!villager.assigned && adjustmentType.Equals("add")) {
                Assign(fixtureObj, villager);
                break;
            } else if (villager.assigned && villager.assignment.id == fixtureObj.id && adjustmentType.Equals("remove")) {
                Unassign(fixtureObj, villager);
                break;
            }
        }
    }

    private void Assign(Fixture fixture, Villager villager) {
        villager.Assign(fixture);
        fixture.Assign(villager);
    }

    private void Unassign(Fixture fixture, Villager villager) {
        if (villager.assignment != null) {
            villager.Unassign();
        }
        fixture.Unassign(villager);
    }

}
