﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StockUpdater : MonoBehaviour {
    Properties props;
    private VillageCenter villageCenter;

    void Start() {
        props = GetComponent<Properties>();
        props.count = 0;
        villageCenter = GameObject.Find("VillageCenter").GetComponent<VillageCenter>();
    }

    void Update() {
        if (villageCenter.stockBucket) {
            props.count = villageCenter.stockBucket.GetStockLevel(props.label);
        } else {
            Debug.Log("stockBucket not yet fully loaded");
        }
    }
}
