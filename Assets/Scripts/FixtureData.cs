using System.Collections;
using System.Collections.Generic;

public class FixtureData {
    string label;
    List<Item> produces;
    List<Item> consumes;
    List<Item> additionalConstructMaterials;
}
