using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[Serializable]
public class Item {
    public string name;
    public int quantity;

    public Item(string _name, int _quantity) {
        name = _name;
        quantity = _quantity;
    }

    public string Repr() {
        return name + ": " + quantity;
    }

    public void Adjust(string type, int amount) {
        if (type == "add") {
            quantity += amount;
        } else if (type == "remove") {
            if (quantity > 0) {
                quantity -= amount;
            }
        }
    }
}
