using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Event {
    public string name;
    public string description;
    public string type;
    public string effect;
    public string target;

    public bool active;
    public int timeActive;
    public int potency;
    private int direction;
    public int duration;
    public int frequency;
    public int lastTriggered;
    public float triggerChance;
    private int minDuration = 500;
    private int maxDuration = 1500;
    private int minPotency = 3;
    private int maxPotency = 8;
    private int minFrequency = 1000;
    private int maxFrequency = 2000;
    private float minTriggerChance = 0.0f;
    private float maxTriggerChance = 0.05f;
    private VillageCenter villageCenter;

    public Event(EventData eventData) {
        name = eventData.label;
        description = eventData.description;
        type = eventData.type;
        effect = eventData.effect;
        target = eventData.target;
        direction = eventData.direction;

        SetDefaults();
    }

    void SetDefaults() {
        villageCenter = GameObject.Find("VillageCenter").GetComponent<VillageCenter>();
        active = false;
        timeActive = 0;
        SetPotency();
        SetDuration();
        frequency = UnityEngine.Random.Range(minFrequency, maxFrequency);
        lastTriggered = 0;
        triggerChance = UnityEngine.Random.Range(minTriggerChance, maxTriggerChance);
    }

    private void SetPotency() {
        potency = direction * UnityEngine.Random.Range(minPotency, maxPotency);
    }

    private void SetDuration() {
        duration = UnityEngine.Random.Range(minDuration, maxDuration);
    }

    public void Trigger() {
        SetPotency();
        SetDuration();
        timeActive = 0;
        active = true;
        lastTriggered = (int)Time.time;
        villageCenter.log.Add("activity", description);
    }

    public void Update() {
        if (!active) {
            return;
        }
        // timeActive += 1 + (int)(SpeedSlider.slider.value / 2);
        timeActive += 1;
        // put this logic in EventHandler
        // if (timeActive > duration) {
        //     Deactivate();
        // }
    }

    public bool HasRunCourse() {
        return timeActive > duration;
    }

    public void Deactivate() {
        villageCenter.log.Add("activity", "conditions return to normal\n");
        active = false;
        timeActive = 0;
    }

    public string DebugRepr() {
        return string.Format("{0:d} | {1:s} | {2:s} | {3:s}", potency, type, effect, name);
    }

    public override string ToString() {
        return string.Format("{0:d} | {1:s} | {2:s} | {3:s}", potency, type, effect, name);
    }

    public string Repr() {
        if (type == "production") {
            return string.Format("{0:s}\n{1:d} {2:s} halted", name, effect, type);
        } else {
            return string.Format("{0:s}\n{1:s}", name, effect);
        }
    }
}
