using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StockBucket : ContentToggler {

    public Dictionary<string, int> stocks = new Dictionary<string, int>();
    public Dictionary<string, int> consumed = new Dictionary<string, int>();
    public Dictionary<string, int> produced = new Dictionary<string, int>();
    public Dictionary<string, Object> prefabs = new Dictionary<string, Object>();
    public ScrollRectHelper scrollRectHelper;
    public Transform stockContent;
    Scene StockCanvasScene;
    private List<string> availableStocks = new List<string>();

    public void Init() {
        scrollRectHelper = ScrollRectHelper.CreateInstance("ScrollRectHelper") as ScrollRectHelper;
        // LoadPrefabs("Stock");
        LoadPrefabs();
        CreateStockRect();
        AddToStock(new Item("wood", 100));
        AddToStock(new Item("food", 100));
        AddToStock(new Item("tool", 100));
        AddToStock(new Item("firewood", 100));
        // GenerateStocks();
    }

    public void LoadPrefabs() {
        prefabs["stock"] = Resources.Load("Prefabs/Stock_2", typeof(GameObject));
        prefabs["stockCanvas"] = Resources.Load("Prefabs/StockCanvas", typeof(GameObject));
    }

    public void CreateStockRect() {
        StockCanvasScene = SceneManager.CreateScene("StockCanvas");
        GameObject instance = Instantiate(prefabs["stockCanvas"]) as GameObject;;
        SceneManager.MoveGameObjectToScene(
            instance, StockCanvasScene
        );
        stockContent = instance.transform.Find("StockRect").Find("Viewport").Find("Content");
        toggler = instance.transform.Find("StockRect").gameObject;
    }

    public bool Adjust(string type, Item item) {
        bool adjusted = false;
        if (type == "add") {
            adjusted = AddToStock(item);
        } else if (type == "remove") {
            adjusted = RemoveFromStock(item);
        }
        return adjusted;
    }

    private bool AddToStock(Item item) {
        bool addedToStock = false;
        if (stocks.ContainsKey(item.name)) {
            stocks[item.name] += item.quantity;
            addedToStock = true;
        } else {
            stocks.Add(item.name, item.quantity);
            addedToStock = true;
        }
        if (addedToStock) {
            if (addedToStock && produced.ContainsKey(item.name)) {
                produced[item.name] += item.quantity;
            } else {
                produced.Add(item.name, item.quantity);
            }
            if (!availableStocks.Contains(item.name)) {
                availableStocks.Add(item.name);
            }
        }
        return addedToStock;
    }

    public bool RemoveFromStock(Item item) {
        bool removedFromStock = false;
        if (stocks.ContainsKey(item.name)) {
            // add logic for handling negative
            stocks[item.name] -= item.quantity;
            removedFromStock = true;
        }
        if (removedFromStock) {
            if (consumed.ContainsKey(item.name)) {
                consumed[item.name] += item.quantity;
            } else {
                consumed.Add(item.name, item.quantity);
            }
        }
        return removedFromStock;
    }

    public float CountStocks() {
        return CountDictValues(stocks);
    }

    private float CountDictValues(Dictionary<string, int> theDict) {
        float theSum = theDict.Sum(x => x.Value);
        return theSum;
    }

    public void UpdateStocks(GameObject fixture) {
        Debug.Log("updating stocks");
        List<Item> fixtureStocks = fixture.GetComponent<Fixture>().GetStocks();
        Debug.Log("fixtureStocks are " + fixtureStocks + "; there are " + fixtureStocks.Count);
        foreach (Item entry in fixtureStocks) {
            Debug.Log("checking if stock can be added from " + fixture + ": " + entry.name);
            if (!availableStocks.Contains(entry.name)) {
                Debug.Log("creating stock from " + fixture + ": " + entry.name);
                availableStocks.Add(entry.name);
            }
        }
        GenerateStocks();
    }

    private void GenerateStocks() {
        foreach (string entry in availableStocks) {
            if (StockExists(entry)) {
                continue;
            }
            Debug.Log("stock entry is " + entry);
            CreateStock(entry);
        }
        scrollRectHelper.SortChildren(stockContent);
    }

    private bool StockExists(string checkStock) {
        bool exists = false;
        foreach(Transform child in stockContent) {
            if (child.GetComponent<Properties>().label.Equals(checkStock)) {
                exists = true;
                break;
            }
        }
        return exists;
    }

    // move CreateX, SetObjectPosition, SetObjectParent, SortChildren, UpdateContentSize into a helper class TransformManager or similar
    private void CreateStock(string stockName) {
        Vector3 stockPos = Vector3.zero;
        stockPos.y = 0;
        Debug.Log("instantiating stock " + stockName);
        GameObject instance = Instantiate(prefabs["stock"], stockPos, Quaternion.identity) as GameObject;

        float yOffset = -stockContent.GetComponent<RectTransform>().sizeDelta.y;
        // float yOffset = villagersContent.GetComponent<RectTransform>().sizeDelta.y;
        scrollRectHelper.SetObjectPosition(instance.transform, scrollRectHelper.GetChildrenCount(stockContent), yOffset);
        // scrollRectHelper.SetObjectPosition(instance.transform, scrollRectHelper.GetChildrenCount(villagersContent), yOffset);
        scrollRectHelper.SetObjectParent(stockContent, instance.transform);
        // scrollRectHelper.SetObjectParent(villagersContent, instance.transform);

        instance.GetComponent<Properties>().label = stockName;
        scrollRectHelper.UpdateContentSize(stockContent, instance);
    }

    public int GetStockLevel(string name) {
        if (stocks.ContainsKey(name)) {
            return stocks[name];
        } else {
            return 0;
        }
    }

    public bool HaveEnough(Item item) {
        if (stocks.ContainsKey(item.name) && stocks[item.name] >= item.quantity) {
            return true;
        }
        return false;
    }

    public void Reset() {
        stocks = new Dictionary<string, int>();
        produced = new Dictionary<string, int>();
        consumed = new Dictionary<string, int>();
        Init();
    }

    public string Repr() {
        string theString = "STOCKS";
        float totalStocks = CountDictValues(stocks);
        float totalProduced = CountDictValues(produced);
        float totalConsumed = CountDictValues(consumed);

        List<string> stockNames = produced.Keys.ToList();

        foreach (string entry in stockNames) {
            theString += string.Format("\nQ:{0:R} - {1:R}% | P:{2:d} - {3:R}% | C:{4:d} - {5:R}% | {6:s}",
                stocks[entry],
                Mathf.Round((stocks[entry] / totalStocks) * 100),
                produced[entry],
                Mathf.Round((produced[entry] / totalProduced) * 100),
                consumed[entry],
                Mathf.Round((consumed[entry] / totalConsumed) * 100),
                entry
            );
        }
        return theString;
    }

}
