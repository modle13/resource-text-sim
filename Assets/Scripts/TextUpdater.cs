using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextUpdater : MonoBehaviour {

    TextMeshProUGUI countText;
    TextMeshProUGUI labelText;
    TextMeshProUGUI timerText;
    GameObject activeObject;
    TextMeshProUGUI activeText;
    GameObject stockedObject;
    TextMeshProUGUI stockedText;
    GameObject materialsAvaiableObject;
    TextMeshProUGUI materialsAvaiableText;
    Properties props;
    GameState gameState;

    float lastTrigger;

    void Awake() {
        lastTrigger = Time.time;
        props = GetComponent<Properties>();
        labelText = transform.Find("Label")?.GetComponent<TextMeshProUGUI>();
        countText = transform.Find("Count")?.GetComponent<TextMeshProUGUI>();
        timerText = transform.Find("Timer")?.GetComponent<TextMeshProUGUI>();
        activeObject = transform.Find("active")?.gameObject;
        activeText = activeObject?.GetComponent<TextMeshProUGUI>();
        stockedObject = transform.Find("stocked")?.gameObject;
        stockedText = stockedObject?.GetComponent<TextMeshProUGUI>();
        materialsAvaiableObject = transform.Find("materialsAvailable")?.gameObject;
        materialsAvaiableText = materialsAvaiableObject?.GetComponent<TextMeshProUGUI>();
        gameState = GameObject.Find("GameState").GetComponent<GameState>();
    }

    void Update() {
        if (labelText != null) {
            labelText.text = props.label;
        }
        if (countText != null) {
            countText.text = props.count.ToString();
        }
        if (timerText != null) {
            UpdateTimer();
        }
        if (activeText != null) {
            UpdateBool("active", activeText);
        }
        if (stockedText != null) {
            UpdateBool("stocked", stockedText);
        }
        if (materialsAvaiableText != null) {
            UpdateBool("materialsAvailable", materialsAvaiableText);
        }

        activeObject?.SetActive(gameState.debug);
        stockedObject?.SetActive(gameState.debug);
        materialsAvaiableObject?.SetActive(gameState.debug);
    }

    void UpdateTimer() {
        // F1 prints first decimal of float
        timerText.text = props.remainingTime.ToString("F1");
    }

    void UpdateBool(string targetBool, TextMeshProUGUI target) {
        bool theBool = props.state.Get(targetBool);
        target.text = Convert.ToInt32(theBool).ToString();
    }
}
