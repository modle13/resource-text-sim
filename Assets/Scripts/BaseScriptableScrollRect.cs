using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using TMPro;

// https://catlikecoding.com/unity/tutorials/object-management/multiple-scenes/

public abstract class BaseScriptableScrollRect : ContentToggler {
    Scene CanvasScene;
    public ScrollRectHelper scrollRectHelper;
    public Transform content;
    public Dictionary<string, Object> prefabs = new Dictionary<string, Object>();
    public string scrollRectType;
    public bool visible;

    public void Init(List<string> objectList, string prefabType, string elementType, List<string> config) {
        scrollRectType = prefabType;
        PreInit(prefabType, config);
        scrollRectHelper = ScrollRectHelper.CreateInstance("ScrollRectHelper") as ScrollRectHelper;
        LoadPrefabs(prefabType);
        CreateRect(prefabType);
        GenerateObjects(objectList, prefabType, elementType, config);
        PostInit(elementType);
    }

    public void LoadPrefabs(string prefabType) {
        prefabs[prefabType + "Canvas"] = Resources.Load("Prefabs/" + prefabType + "Canvas", typeof(GameObject));
        prefabs[prefabType] = Resources.Load("Prefabs/" + prefabType, typeof(GameObject));
    }

    public void CreateRect(string prefabType) {
        CanvasScene = SceneManager.CreateScene(prefabType + "Canvas");
        GameObject instance = Instantiate(prefabs[prefabType + "Canvas"]) as GameObject;
        SceneManager.MoveGameObjectToScene(
            instance, CanvasScene
        );
        toggler = instance.transform.Find("ScrollRect").gameObject;
        content = toggler.transform.Find("Viewport").Find("Content");
        float contentWidth = content.GetComponent<RectTransform>().sizeDelta.x;
        content.position = (new Vector3(contentWidth / 2, content.position.y, content.position.z));
    }

    public virtual void PreInit(string prefabType, List<string> config) {
        Debug.Log("no custom pre init to invoke");
    }
    public abstract void GenerateObjects(List<string> objectList, string prefabType, string elementType, List<string> config);
    public virtual void PostInit(string elementType) {
        Debug.Log("no custom post init to invoke");
    }

    public Transform GetContent() {
        return content;
    }

    public int GetCount() {
        return content.childCount;
    }
}
