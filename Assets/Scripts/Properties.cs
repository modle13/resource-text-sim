using UnityEngine;
using System;

// this class is being used for both villager and building properties
// this violates SRP
// use a base abstract class?
public class Properties : MonoBehaviour {
    public int count;
    public string label;
    public bool active;
    public float remainingTime;
    public State state = new State();

    public void Adjust(string type) {
        if (type == "add") {
            count += 1;
        } else if (type == "remove") {
            if (count > 0) {
                count -= 1;
            }
        }
    }

    public void Update() {
        active = count > 0;
    }
}

[Serializable]
public class State {
    public bool active;
    public bool stocked;
    public bool materialsAvailable;

    public bool Get(string target) {
        if (target == "active") {
            return active;
        }
        if (target == "stocked") {
            return stocked;
        }
        if (target == "materialsAvailable") {
            return materialsAvailable;
        }
        return false;
    }
}
