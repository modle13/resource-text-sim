using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

[Serializable]
public class Stat {
    public string name;
    public int minValue;
    public int maxValue;
    public int value;
    public Item consumes;
    public string condition;
    public int conditionTrigger = 0;
    public int consumeAmount = 1;
    public int consumeTrigger = 1;
    public int replenishValue = 99;

    public Stat(StatData _data) {
        name = _data.name;
        minValue = _data.minValue;
        maxValue = _data.maxValue;
        value = _data.maxValue;
        consumes = new Item(_data.consumes, _data.consumeAmount);
        condition = _data.condition;
    }

    public Stat(string _name, int _minValue, int _maxValue, Item _consumes, string _condition) {
        name = _name;
        minValue = _minValue;
        maxValue = _maxValue;
        value = _maxValue;
        consumes = _consumes;
        condition = _condition;
    }

    public void AdjustValue(int amount) {
        this.value += (this.value > this.minValue ? amount : 0);
    }

    public void Replenish() {
        AdjustValue(replenishValue);
    }

    public bool IsTriggered() {
        return value < consumeTrigger;
    }
}
