public class VillagerData {
    public int villagerCount;
    public int deathTrigger;
    public int maxStatValue;
    public int minStatValue;
    public int clothingMultiplier;
    public int warmthWorkMultiplier;
    public int warmthIdleMultiplier;
    public int baseStatDelta;
}
