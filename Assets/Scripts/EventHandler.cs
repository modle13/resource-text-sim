using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class EventHandler : MonoBehaviour {

    public Event currentEvent;
    public int timeSinceEvent = 0;
    private int minTimeBetweenEvents = 300;
    private Dictionary<string, string> availableEntities = new Dictionary<string, string>();
    private List<EventData> initialEntities = new List<EventData>();
    private List<Event> events = new List<Event>();
    private FileHandler fileHandler;
    private bool loaded;

    public void Start() {
        fileHandler = GameObject.Find("FileHandler").GetComponent<FileHandler>();
    }

    public void Update() {
        if (!loaded) {
            Debug.Log("events_debug: attempting to load events");
            LoadEvents();
            return;
        }
        if (currentEvent == null) {
            Debug.Log("events_debug: setting default event");
            SetDefaultEvent();
            return;
        }
        if (loaded && currentEvent.active) {
            // Debug.Log("events_debug: checking for deactivation");
            CheckForDeactivation();
        } else {
            // Debug.Log("events_debug: managing events");
            Manage();
        }
    }

    private void LoadEvents() {
        if (fileHandler.data.ContainsKey("events") && fileHandler.data["events"].Count != 0 && !loaded) {
            Debug.Log("events_debug: event data is loaded in fileHandler; reading from json");
            foreach (KeyValuePair<string, string> entry in fileHandler.data["events"]) {
                Debug.Log("current event to be loaded is " + entry.Value);
                if (entry.Value != "") {
                    EventData data = JsonUtility.FromJson<EventData>(entry.Value);
                    events.Add(new Event(data));
                }
            }
            foreach (Event e in events) {
                Debug.Log("Loaded event " + e.ToString());
            }
            loaded = true;
        }
    }

    private void SetDefaultEvent() {
        SetEvent();
        currentEvent.active = false;
    }

    private void SetEvent() {
        currentEvent = events[Random.Range(0, events.Count)];
        Debug.Log("events_debug: setting current event to " + currentEvent);
    }

    public void Manage() {
        if (timeSinceEvent < minTimeBetweenEvents) {
            timeSinceEvent += 1;
            // Debug.Log("events_debug: managing events: not enough time has passed, returning");
            return;
        }
        Debug.Log("events_debug: enough time has passed; calling ActivateEvent");
        ActivateEvent();
    }

    private void ActivateEvent() {
        Debug.Log("events_debug: activating event");
        SetEvent();
        currentEvent.Trigger();
    }

    private void CheckForDeactivation() {
        currentEvent.Update();
        // Debug.Log(currentEvent.timeActive + " " + currentEvent.duration);
        if (currentEvent.HasRunCourse()) {
            currentEvent.Deactivate();
            // this is why the deactivate management has to live here
            // don't want Event calling EventHandler
            timeSinceEvent = 0;
        }
    }

    public int GetEventEffect(string testTarget, string testType) {
        if (currentEvent.active && currentEvent.target == testTarget && currentEvent.type == testType) {
            return currentEvent.potency;
        } else {
            return 0;
        }
    }

    // used by buildings to see if any of their production items are blocked by events
    public bool HasBlockingEvent(List<Item> testTargets, string testType) {
        int eventEffect = 0;
        foreach (Item entry in testTargets) {
            eventEffect = GetEventEffect(entry.name, testType);
            if (eventEffect != 0) {
               break;
            }
        }
        return eventEffect != 0;
    }

    // public void Reset() {
    //     SetDefaults();
    //     SetDefaultEvent();
    // }
}
