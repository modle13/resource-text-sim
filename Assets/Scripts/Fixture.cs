using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

public class Fixture : MonoBehaviour {

    public int id;
    private bool loaded;
    public bool salvage = false;
    public bool convert = false;
    public string phase = "standard";
    public string elementType;
    public string nextForm = "";
    private Properties props;
    private float timeToProduce = 4f;
    private float lastUpdate;
    public List<Item> produce = new List<Item>();
    public List<Item> consume = new List<Item>();
    public List<Item> construct = new List<Item>();
    List<Villager> villagers = new List<Villager>();
    private VillageCenter villageCenter;

    private int productionCeiling = 10;
    private int productionMinimum = 1;
    private int productionRateIncrease = 20;
    private EventHandler eventHandler;
    private FileHandler fileHandler;

    public void Awake() {
        props = GetComponent<Properties>();
        lastUpdate = Time.time;
        props.remainingTime = timeToProduce;
        SetDefaultState();
        villageCenter = GameObject.Find("VillageCenter").GetComponent<VillageCenter>();
    }

    public void Start() {
        eventHandler = GameObject.Find("EventHandler").GetComponent<EventHandler>();
        fileHandler = GameObject.Find("FileHandler").GetComponent<FileHandler>();
    }

    public void Update() {
        props.count = villagers.Count;
        ManageProductionState();
        if (!loaded) {
            Initialize();
        }
    }

    public void Initialize() {
        if (fileHandler.data.ContainsKey(elementType) && fileHandler.data[elementType].Count != 0 && !loaded && props.label != null) {
            Debug.Log("initializing " + props.label + "; elementType is " + elementType);
            foreach (KeyValuePair<string, string> entry in fileHandler.data[elementType]) {
                Debug.Log("initializing: key is " + entry.Key + "; value is " + entry.Value);
            }
            LoadItems("consume", fileHandler.data[elementType], consume);
            LoadItems("produce", fileHandler.data[elementType], produce);
            LoadItems("construct", fileHandler.data[elementType], construct);
            if (phase.Equals("worksite")) {
                ConfigureWorksite();
            }
            loaded = true;
        }
    }

    private void SetDefaultState() {
        props.state.active = false;
        props.state.stocked = false;
        props.state.materialsAvailable = false;
    }

    private void LoadItems(string type, Dictionary<string, string> rawData, List<Item> bucket) {
        string dataKey = props.label + "_" + type + ".json";
        Debug.Log("initializing: data key is " + dataKey);
        string jsonString = "";
        try {
            jsonString = rawData[dataKey];
            Debug.Log("initializing: jsonString is " + jsonString);
        } catch (Exception e) {
            Debug.Log("initializing: no " + dataKey + " found");
        }
        if (jsonString != "") {
            Item[] loadedItems = JsonHelper.FromJson<Item>(jsonString);
            bucket.AddRange(loadedItems);
        }
        foreach (Item item in bucket) {
            Debug.Log("initializing: item is " + item.name);
        }
    }

    private void ConfigureWorksite() {
        SetLabel("worksite - " + GetLabel());
        consume = construct;

        string consumeItemsString = "";
        foreach (Item consumeItem in consume) {
            consumeItemsString += "; " + consumeItem.Repr();
        }
        Debug.Log("consumeItems entries after conversion to worksite: " + consumeItemsString);
    }

    public void ManageProductionState() {
        CheckFixtureConditions();
        HandleConsumption();
        UpdateProgress();
        ProcessProductionCompletion();
    }

    private void CheckFixtureConditions() {
        props.state.active = IsActive();
        if (!props.state.stocked && !props.state.materialsAvailable) {
            props.state.materialsAvailable = AreMaterialsAvailable();
        }
    }

    private bool CanConsume() {
        return props.state.active && !props.state.stocked && props.state.materialsAvailable && !salvage && !convert;
    }

    private bool IsActive() {
        return props.count > 0;
    }

    private bool AreMaterialsAvailable() {
        bool available = true;
        if (consume.Count == 0) {
            return available;
        }
        foreach (Item entry in consume) {
            if (!available) {
                break;
            }
            // are we going to have to worry about overconsumption if these are all running in parallel?
            // they go negative, just a bit
            available = villageCenter.stockBucket.HaveEnough(entry);
        }
        return available;
    }

    private void HandleConsumption() {
        if (!CanConsume()) {
            return;
        }

        // we've already checked whether materials are available at this point
        foreach (Item entry in consume) {
            // actually consume
            villageCenter.stockBucket.Adjust("remove", entry);
            // StoreHandler.handler.stores[entry.Name].Consume(entry.quantity);
            villageCenter.log.Add("production", "consumed " + entry.quantity + " " + entry.name);
        }
        props.state.stocked = true;
        props.state.materialsAvailable = false;
    }

    public bool IsProducing() {
        return !IsBlockedByEvent() && props.state.active && props.state.stocked;
    }

    private bool IsBlockedByEvent() {
        return eventHandler.HasBlockingEvent(produce, "production");
    }

    public void UpdateProgress() {
        if (IsProducing()) {
            // calculations: https://docs.google.com/spreadsheets/d/1wGjc8fsEU4NBejf4CDlVwSjS_RAe8uPdO-iGDFyPZnU/edit?usp=sharing
            // ((x-1)*a)/((x-1)+b)+c
            int coefficient = props.count - 1;
            float productionMod = (coefficient * productionCeiling) / (coefficient + productionRateIncrease) + productionMinimum;
            float updateDiff = (Time.time - lastUpdate) * productionMod;
            props.remainingTime -= updateDiff;
        }
        lastUpdate = Time.time;
    }

    private void ProcessProductionCompletion() {
        if (!IsProducing()) {
            Debug.Log("not producing");
            return;
        }
        if (props.remainingTime <= 0.0f) {
            Debug.Log("Production is ready for " + props.label);
            // discard overflow if production cannot continue
            props.state.stocked = false;
            props.remainingTime = timeToProduce;
            if (phase.Equals("worksite") && !salvage) {
                // worksites have special production behavior
                // the thing they produce is a new fixture rather than what's in `produce`
                // let VillageCenter do the cleanup/create, and just set a status on this fixture
                convert = true;
                return;
            }
            foreach (Item entry in produce) {
                Debug.Log("producing " + entry);
                // // Score not implemented yet
                // Score.score.Add(1);
                villageCenter.stockBucket.Adjust("add", entry);
                villageCenter.log.Add("production", "Job's done: " + props.label + " made " + entry.quantity + " " + entry.name);
            }
        }
    }

    public void Assign(Villager villager) {
        Debug.Log("Assigned villager");
        villagers.Add(villager);
    }

    public void UnassignAll() {
        foreach (Villager villager in villagers) {
            villager.Unassign();
        }
        villagers = new List<Villager>();
    }

    public void Unassign(Villager villager) {
        Villager target = villagers.SingleOrDefault(x => x.id == villager.id);
        if (target != null) {
            villagers.Remove(target);
        }
        Debug.Log("Unassigned villager");
    }

    public string GetLabel() {
        return props.label;
    }

    public void SetLabel(string newLabel) {
        props.label = newLabel;
    }

    public string Repr() {
        return string.Format(
            "V:{0:d} | T:{1:d} | {2:s} | P:{3:f}",
            props.count, 0, props.label, props.remainingTime / timeToProduce
        );
    }

    public List<Item> GetStocks() {
        Debug.Log("In GetStocks");
        List<Item> combinedStocks = new List<Item>();
        combinedStocks.AddRange(consume);
        combinedStocks.AddRange(produce);
        combinedStocks.AddRange(construct);
        Debug.Log("combinedStocks length is " + combinedStocks.Count);
        foreach (Item stock in combinedStocks) {
            Debug.Log("got stock " + stock.name);
        }
        return combinedStocks;
    }
}
