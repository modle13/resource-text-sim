using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// https://catlikecoding.com/unity/tutorials/object-management/multiple-scenes/

[CreateAssetMenu]
public class ScrollRectHelper : ScriptableObject {

    float topOffset = 31f;
    float spaceUsedByMenu = 300;

    public void UpdateContentSize(Transform parent, GameObject myObject) {
        // content field needs to adjust with # of buildings
        float objectHeight = myObject.GetComponent<RectTransform>().sizeDelta.y;
        float expectedHeight = objectHeight * parent.childCount;
        RectTransform rt = parent.GetComponent<RectTransform>();
        if (rt.sizeDelta.y < expectedHeight) {
            // get original top position
            float parentTop = rt.offsetMax.y;
            // resize it, but this changes the top position
            rt.sizeDelta = new Vector2(rt.sizeDelta.x, expectedHeight + topOffset * 2);
            // readjust top position, give it a vertical offset
            rt.offsetMax = new Vector2(rt.offsetMax.x, parentTop + topOffset);
        }
    }

    public void SetObjectPosition(Transform myTransform, float yPosMultiplier, float yOffset) {
        Debug.Log("SetObjectPosition called with yOffset of " + yOffset);
        Vector3 originalSize = myTransform.GetComponent<RectTransform>().sizeDelta;
        float verticalOffset = originalSize.y / 2;

        // handles offsetting multiple instances
        // distance between should be the height of the object
        // position should start at 0
        float yPos = (-1 * yPosMultiplier * originalSize.y) - verticalOffset;
        myTransform.position = (new Vector3(originalSize.x / 2, yPos, myTransform.position.z));
    }

    public void SetObjectParent(Transform parent, Transform myTransform) {
        // to take advantage of ignored relative positioning on sort
        // myTransform.SetParent(null);
        // false ignores relative parent position
        // myTransform.SetParent(parent);
        myTransform.SetParent(parent, false);
    }

    // this function call causes objects to move way off the screen
    public void SortChildren(Transform parent) {
        // get the transforms
        List<Transform> tempTransforms = new List<Transform>();
        int currentCount = 0;
        foreach (Transform t in parent) {
            if (t.GetComponent<Properties>() == null) {
                 throw new Exception("Can't sort " + parent.name + ": Object does not have Properties");
            }
            tempTransforms.Add(t);
            Debug.Log("SetObjectPosition call for " + t.GetComponent<Properties>().label);
            SetObjectPosition(t, currentCount, 0);
            SetObjectParent(parent, t);
            currentCount += 1;
        }
        // sort them by label
        // tempTransforms.Sort(delegate(Transform x, Transform y) {
        //     return x.GetComponent<Properties>().label.CompareTo(y.GetComponent<Properties>().label);
        // });
        // reset their positions in order
        // this is causing them to be set at 50000 Y Pos
        // int currentCount = 0;
        // foreach (Transform t in tempTransforms) {
        //     SetObjectPosition(t, currentCount, 0);
        //     SetObjectParent(parent, t);
        //     currentCount += 1;
        // }
    }

    public int GetChildrenCount(Transform parent) {
        return parent.childCount;
    }
}
