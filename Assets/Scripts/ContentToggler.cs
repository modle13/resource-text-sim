using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using TMPro;

public abstract class ContentToggler : ScriptableObject {

    public GameObject toggler;

    public void SetContentView(string state) {
        toggler.SetActive(state.Equals("on"));
    }

    public void ToggleContentView() {
        toggler.SetActive(!toggler.activeSelf);
    }
}
