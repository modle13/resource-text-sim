using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class MapVillager : MonoBehaviour {

    private int lastChange = 100;
    private int timeToNextChange = 100;
    private Vector2 direction;
    Transform container;

    public void Awake() {
        container = transform.parent;
        direction = new Vector2(0, 0);
    }

    public void Update() {
        if (CanChangeDirection()) {
            GetDirection();
        }
        Move();
    }

    private bool CanChangeDirection() {
        bool canChange = false;
        lastChange++;
        if (lastChange > timeToNextChange) {
            canChange = true;
            lastChange = 0;
        }
        return canChange;
    }

    private void GetDirection() {
        float newX = Random.Range(-1.0f, 1.0f);
        float newY = Random.Range(-1.0f, 1.0f);
        direction = new Vector2(newX, newY);
    }

    private void Move() {
        Vector2 currentPos = new Vector2(transform.localPosition.x, transform.localPosition.y);
        if (!transform.parent.GetComponent<RectTransform>().rect.Contains(currentPos + direction)) {
            return;
        }
        transform.localPosition += (Vector3)direction;
    }
}
