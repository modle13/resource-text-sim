using System.Collections;
using System.Collections.Generic;

public static class Config {
    // make these lists instead, and do a List contains "somestring"
    // this makes configuration easier
    public static Dictionary<string, string> dataCollectionTypes = new Dictionary<string, string>() {
        {"buildings", "listOfDirs"},
        {"events", "listOfFiles"},
        {"research", "listOfDirs"},
        {"stats", "listOfFiles"},
        {"villagerData", "singleFile"},
        {"villagerNames", "singleFile"},
    };

    public static Dictionary<string, List<string>> dataLists = new Dictionary<string, List<string>>();
    public static Dictionary<string, string> data = new Dictionary<string, string>() {
        {"buildings", "data/building"},
        {"events", "data/events"},
        {"research", "data/research"},
        {"stats", "data/villageCenter/stats"},
        {"villagerData", "data/villageCenter/villagers.json"},
        {"villagerNames", "data/villageCenter/villagerNames.txt"},
    };
    public static Dictionary<string, List<string>> expectedFiles = new Dictionary<string, List<string>>() {
        {"buildings", new List<string>(new string[] {"consume.json", "produce.json", "construct.json"})},
        {"research", new List<string>(new string[] {"consume.json", "produce.json", "construct.json"})}
    };
    public static List<string> buildings = new List<string>(
        new string[] {
            "chandler",
            "forester",
            "gatherer",
            "hunter",
            "miner",
            "shipwright",
            "smith",
            "tailor",
            "woodcutter"
        }
    );
    public static List<string> events = new List<string>(
        new string[] {
            "acidRain.json",
            "caveIn.json",
            "famine.json",
            "forestFire.json",
            "maintenance.json",
            "monsoon.json",
            "mutantMoths.json",
            "perfectWeather.json",
            "scarceResources.json",
            "snowStorm.json"
        }
    );
    public static List<string> research = new List<string>(
        new string[] {
            "automatons",
            "asdf"
        }
    );
    public static List<string> stats = new List<string>(
        new string[] {
            "clothing.json",
            "energy.json",
            "tool.json",
            "warmth.json"
        }
    );
    public static void Init() {
        dataLists.Add("buildings", buildings);
        dataLists.Add("events", events);
        dataLists.Add("research", research);
        dataLists.Add("stats", stats);
    }
}
