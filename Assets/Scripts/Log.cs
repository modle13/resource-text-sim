using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class Log : ContentToggler {

    Scene LogScene;

    private static TextMeshProUGUI theText;
    private static TextMeshProUGUI productionText;
    private static TextMeshProUGUI activityText;

    Transform container;

    private int maxEntryCount = 15;
    public List<string> productionEntries = new List<string>();
    public List<string> activityEntries = new List<string>();

    public void Init() {
        CreateRect();
        SetDefaults();
    }

    private void CreateRect() {
        LogScene = SceneManager.CreateScene("LogCanvas");
        Object logCanvas = Resources.Load("Prefabs/LogCanvas", typeof(GameObject));
        GameObject instance = Instantiate(logCanvas) as GameObject;
        container = instance.transform.Find("Logs");
        toggler = container.gameObject;
        productionText = container.Find("Production").GetComponent<TextMeshProUGUI>();
        activityText = container.Find("Activity").GetComponent<TextMeshProUGUI>();
        SceneManager.MoveGameObjectToScene(
            instance, LogScene
        );
    }

    private void SetDefaults() {
        AddStartingMessages();
    }

    private void AddStartingMessages() {
        string target = "activity";
        Add(target, "==============================");
        Add(target, "");
        Add(target, "Good luck!");
        Add(target, "");
        Add(target, "Assign villagers to buildings to produce the resources they need to stay alive.");

        Add(target, "==============================");
        Add(target, "");
        Add(target, "Welcome to Resource Quest!");
        Add(target, "");
        Add(target, "==============================");
        Add(target, "");
        Add(target, "");
    }

    public void Add(string target, string entry) {
        if (target.Equals("activity")) {
            activityEntries.Insert(0, entry.ToString());
        }
        if (target.Equals("production")) {
            productionEntries.Insert(0, entry.ToString());
        }
    }

    public void Update() {
        Cleanup(productionEntries);
        Cleanup(activityEntries);
        if (productionEntries.Count > 0) {
            SetText(productionEntries, productionText);
        }
        if (activityEntries.Count > 0) {
            SetText(activityEntries, activityText);
        }
    }

    private void Cleanup(List<string> entries) {
        while (entries.Count > maxEntryCount) {
            entries.RemoveAt(entries.Count - 1);
        }
    }

    private void SetText(List<string> entries, TextMeshProUGUI targetUIelement) {
        string joinedText = "";
        foreach (string entry in entries) {
            joinedText += entry + "\n";
        }
        targetUIelement.text = joinedText;
    }

    public void Reset() {
        SetDefaults();
    }
}
