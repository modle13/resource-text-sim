using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using TMPro;

// https://catlikecoding.com/unity/tutorials/object-management/multiple-scenes/

[CreateAssetMenu]
public class ButtonBucket : BaseScriptableScrollRect {

    public override void GenerateObjects(List<string> objectList, string prefabType, string elementType, List<string> config) {
        Debug.Log("generating button bucket objects");
        foreach (string entry in objectList) {
            CreateObject(entry, prefabType);
        }
    }

    private GameObject CreateObject(string name, string prefabType) {
        Vector3 pos = Vector3.zero;
        GameObject instance = Instantiate(prefabs[prefabType], pos, Quaternion.identity) as GameObject;

        scrollRectHelper.SetObjectPosition(instance.transform, scrollRectHelper.GetChildrenCount(content), 0);
        scrollRectHelper.SetObjectParent(content, instance.transform);

        // needed for sort
        instance.GetComponent<Properties>().label = name;

        // scrollRectHelper.UpdateContentSize(content, instance);
        // sort is broken, throws the objects off the screen
        // scrollRectHelper.SortChildren(content);
        return instance;
    }
}
