using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

[CreateAssetMenu]
public class VillageMap : ContentToggler {

    Scene VillageMapScene;
    VillageCenter villageCenter;

    public Transform buildingMap;
    public Transform stockMap;
    public Transform villagerMap;

    Transform container;

    bool initialized = false;

    private TextMeshProUGUI buildingCount;
    private TextMeshProUGUI stockCount;
    private TextMeshProUGUI villagerCount;

    public Dictionary<string, Object> prefabs = new Dictionary<string, Object>();

    public void Init() {
        LoadPrefabs();
        CreateRect();
        villageCenter = GameObject.Find("VillageCenter").GetComponent<VillageCenter>();

        buildingCount = buildingMap.GetComponent<TextMeshProUGUI>();
        stockCount = stockMap.GetComponent<TextMeshProUGUI>();
        villagerCount = villagerMap.GetComponent<TextMeshProUGUI>();
        Debug.Log("initialized village map");
        initialized = true;
    }

    private void CreateRect() {
        VillageMapScene = SceneManager.CreateScene("VillageMapCanvas");
        GameObject instance = Instantiate(prefabs["VillageMapCanvas"]) as GameObject;
        container = instance.transform.Find("VillageMap");
        toggler = container.gameObject;
        buildingMap = container.Find("Buildings");
        stockMap = container.Find("Stocks");
        villagerMap = container.Find("Villagers");
        SceneManager.MoveGameObjectToScene(
            instance, VillageMapScene
        );
    }

    private void LoadPrefabs() {
        prefabs["VillageMapCanvas"] = Resources.Load("Prefabs/VillageMapCanvas", typeof(GameObject));
        prefabs["building"] = Resources.Load("Prefabs/MapBuilding", typeof(GameObject));
        prefabs["stock"] = Resources.Load("Prefabs/MapStock", typeof(GameObject));
        prefabs["villager"] = Resources.Load("Prefabs/MapVillager", typeof(GameObject));
    }

    public void Update() {
        // add cleanup conditions; delete map representations when village objects are destroyed
        if (!initialized) {
            return;
        }
        if (villageCenter.buildingBucket != null) {
            buildingCount.text = "Buildings: " + villageCenter.buildingBucket.GetCount();
            if (villageCenter.buildingBucket.GetCount() > buildingMap.childCount) {
                Spawn("building", buildingMap);
            }
        }
        if (villageCenter.villagerBucket != null) {
            villagerCount.text = "Villagers: " + villageCenter.villagerBucket.GetCount();
            if (villageCenter.villagerBucket.GetCount() > villagerMap.childCount) {
                Spawn("villager", villagerMap);
            }
        }
        if (villageCenter.stockBucket != null) {
            stockCount.text = "Stocks: " + villageCenter.stockBucket.CountStocks();
            if (villageCenter.stockBucket.CountStocks() > stockMap.childCount) {
                Spawn("stock", stockMap);
            }
        }
    }

    private void Spawn(string type, Transform parent) {
        Vector3 spawnPos = GetRandomPos(parent);
        GameObject instance = Instantiate(prefabs[type], spawnPos, Quaternion.identity) as GameObject;
        SetObjectParent(parent, instance.transform);
    }

    private Vector3 GetRandomPos(Transform parent) {
        Vector2 dimensions = parent.GetComponent<RectTransform>().sizeDelta;
        Rect parentRect = parent.GetComponent<RectTransform>().rect;
        float maxX = dimensions.x;
        float maxY = dimensions.y;
        float minX = 0;
        float minY = 0;
        float selectedX = Random.Range(minX, maxX) - parentRect.width / 2;
        float selectedY = Random.Range(minY, maxY) - parentRect.height / 2;
        Vector3 selectedPos = new Vector3(selectedX, selectedY, 0);
        return selectedPos;
    }

    private void SetObjectPosition(Transform myTransform, float yPosMultiplier) {
        Vector3 originalSize = myTransform.GetComponent<RectTransform>().sizeDelta;
        float yPos = -20 + yPosMultiplier * -originalSize.y;
        myTransform.position = (new Vector3(originalSize.x / 2, yPos, myTransform.position.z));
    }

    private void SetObjectParent(Transform parent, Transform myTransform) {
        // to take advantage of ignored relative positioning on sort
        myTransform.SetParent(null);
        // false ignores relative parent position
        myTransform.SetParent(parent, false);
    }

}
